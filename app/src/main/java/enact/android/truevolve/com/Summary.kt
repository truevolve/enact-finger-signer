package enact.android.truevolve.com

import android.graphics.BitmapFactory
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.ImageView
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.PolicyException
import enact.android.truevolve.com.enact_screen_signature.R
import org.json.JSONException
import org.json.JSONObject

class Summary : ActivityBaseController() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.summary_layout)

        val data = Interpreter.dataStore
        val done = findViewById<Button>(R.id.doneButton)
        val reviewPicture = findViewById<View>(R.id.takenPicture) as ImageView

        try {
            val pictureData = stateObj?.getString(RETRIEVE_AS)?.let { data.getProperty<ByteArray>(it) }
            val bmp = pictureData?.size?.let { BitmapFactory.decodeByteArray(pictureData, 0, it) }
            reviewPicture.setImageBitmap(bmp)

        } catch (e: JSONException) {
            trikita.log.Log.e("error retrieving signature: ", e)
        }


        done.setOnClickListener {
            try {
                stateObj?.getString(ON_DONE)?.let { goToState(it) }
            } catch (e: JSONException) {
                trikita.log.Log.e("error exiting summary: ", e)
            }
        }


    }

    override val type = TYPE

    override fun validate(stateObj: JSONObject) {
        if (!stateObj.has(ON_DONE)) {
            Log.e(TAG, "validate: state object must have $ON_DONE defined")
            throw PolicyException("State object must have $ON_DONE defined")
        } else if (!stateObj.has(RETRIEVE_AS)) {
            Log.e(TAG, "validate: state object must have $RETRIEVE_AS defined")
            throw PolicyException("State object must have $RETRIEVE_AS defined")
        } else if (!stateObj.has(DISPLAY_MESSAGE)) {
            Log.e(TAG, "validate: state object must have $DISPLAY_MESSAGE defined")
            throw PolicyException("State object must have $DISPLAY_MESSAGE defined")
        }
    }

    companion object {
        private const val TYPE = "summary"
        private const val TAG = "Summary"
        private const val ON_DONE = "on_done"
        private const val DISPLAY_MESSAGE = "display_message"
        private const val RETRIEVE_AS = "retrieve_as"
    }
}