package enact.android.truevolve.com

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.truevolve.android.enact.signature.Signature
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import enact.android.truevolve.com.enact_screen_signature.R

class MainActivity : AppCompatActivity() {


    private val policy = """
            {
                "start": {
                    "on_start":"finger_signer",
                    "type": "menu"
                },
                "finger_signer": {
                    "type": "finger_signer",
                    "store_as":"signature_object",
                    "on_cancel": "start",
                    "on_next":"summary",
                    "display_message":"Provide signature below",
                    "next_button_text":"Submit",
                    "cancel_button_text":"Cancel",
                    "clear_button_text":"Clear",
                    "disclaimer_short_text":"I agree to the disclaimer",
                    "accept_terms_text":"I promise to follow the rules and behave accordingly for the duration of my visit to these premises"
                },
                "summary":{
                    "type": "summary",
                    "retrieve_as": "signature_object",
                    "display_message": "Here is your signature",
                    "on_done": "end"
                }
            }
        """

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //validate if an intent has error and end
        if (intent.hasExtra(Interpreter.END)) {
            Log.d(TAG, "onCreate: ENDED")
        }

        if (intent.hasExtra(Interpreter.ERROR)) {
            Log.d(TAG, "onCreate: ERRORED")
            val throwable = intent.getSerializableExtra("exception") as Throwable
            Log.e(TAG, "onCreate: Errored: ", throwable)
        }


        val listOfControllers = ArrayList<Class<out ActivityBaseController>>()
        listOfControllers.add(Menu::class.java)
        listOfControllers.add(Summary::class.java)
        listOfControllers.add(Signature::class.java)

        try {
            val interpreter = Interpreter.setup(MainActivity::class.java, policy, listOfControllers)
            interpreter.start(this)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume: RESUMING")
    }

    companion object {
        private const val TAG = "MainActivity"
    }

}