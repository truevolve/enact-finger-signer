package enact.android.truevolve.com

import android.os.Bundle
import android.util.Log
import android.view.View
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.InterpreterException
import com.truevolve.enact.exceptions.PolicyException
import enact.android.truevolve.com.enact_screen_signature.R
import org.json.JSONException
import org.json.JSONObject

class Menu : ActivityBaseController() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        try {

            findViewById<View>(R.id.buttonStart).setOnClickListener {
                try {
                    stateObj?.getString(ON_START)?.let { goToState(it) }
                } catch (e: InterpreterException) {
                    e.printStackTrace()
                    Interpreter.error(this@Menu, e)
                } catch (e: JSONException) {
                    e.printStackTrace()
                    Interpreter.error(this@Menu, e)
                }
            }

        } catch (e: JSONException) {
            e.printStackTrace()
        }

    }


    override fun validate(jsonObj: JSONObject) {
        if (!jsonObj.has(ON_START)) {
            Log.e(TAG, "validate: state object does not have $ON_START which is mandatory")
            throw PolicyException("State object does not have $ON_START which is mandatory")
        }
    }

    override val type = TYPE


    companion object {
        private val ON_START = "on_start"
        private val TAG = "Menu"
        private const val TYPE = "menu"
    }


}