package com.truevolve.android.enact.signature

import android.app.AlertDialog
import android.graphics.Bitmap
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.github.gcacace.signaturepad.views.SignaturePad
import com.truevolve.enact.Interpreter
import com.truevolve.enact.controllers.ActivityBaseController
import com.truevolve.enact.exceptions.PolicyException
import enact.android.truevolve.com.signature.R
import org.json.JSONException
import org.json.JSONObject
import trikita.log.Log
import java.io.ByteArrayOutputStream


class Signature : ActivityBaseController() {

    private lateinit var signaturePad: SignaturePad

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.signature_layout)
        signaturePad = findViewById(R.id.signature_pad)

        val saveButton = findViewById<Button>(R.id.buttonSave)
        val cancelButton = findViewById<Button>(R.id.buttonCancel)
        val clearButton = findViewById<Button>(R.id.buttonClear)
        val instructionText = findViewById<TextView>(R.id.instructionTV)
        val disclaimerLink = findViewById<TextView>(R.id.disclaimerLink)
        val disclaimerSummary = findViewById<TextView>(R.id.disclaimerSummary)

        if (stateObj?.has(ACCEPT_TERMS_TEXT) == false) {
            disclaimerLink.visibility = View.GONE
        }

        disclaimerLink.setOnClickListener {
            showDisclaimerDialog()
        }

        try {

            if (stateObj?.has(DISPLAY_MESSAGE) == true) {
                instructionText.text = stateObj?.getString(DISPLAY_MESSAGE)
            }

            if (stateObj?.has(NEXT_BUTTON_TEXT) == true) {
                saveButton.text = stateObj?.getString(NEXT_BUTTON_TEXT)
            }

            if (stateObj?.has(CANCEL_BUTTON_TEXT) == true) {
                cancelButton.text = stateObj?.getString(CANCEL_BUTTON_TEXT)
            }

            if (stateObj?.has(CLEAR_BUTTON_TEXT) == true) {
                clearButton.text = stateObj?.getString(CLEAR_BUTTON_TEXT)
            }

            if (stateObj?.has(DISCLAIMER_SHORT_TEXT) == true) {
                disclaimerSummary.text = stateObj?.getString(DISCLAIMER_SHORT_TEXT)
            }

            saveButton.setOnClickListener {

                if (!signaturePad.isEmpty) {
                    val imageBitMap = signaturePad.signatureBitmap
                    val baos = ByteArrayOutputStream()
                    imageBitMap.compress(Bitmap.CompressFormat.PNG, 0, baos)
                    val bitmapByteArray = baos.toByteArray()

                    stateObj?.getString(STORE_AS)?.let { Interpreter.dataStore.put(it, bitmapByteArray) }
                    stateObj?.getString(ON_NEXT)?.let { goToState(it) }
                } else {
                    Snackbar.make(it, resources.getText(R.string.no_signature_added), Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show()
                }
            }

            cancelButton.setOnClickListener {
                try {
                    stateObj?.getString(ON_CANCEL)?.let { goToState(it) }

                } catch (e: JSONException) {
                    e.printStackTrace()
                }
            }

            clearButton.setOnClickListener {
                signaturePad.clear()
            }
        } catch (e: JSONException) {
            e.printStackTrace()
        }
    }


    private fun showDisclaimerDialog() {
        val dialogBuilder = AlertDialog.Builder(this)
        val inflater = this.layoutInflater
        val dialogView = inflater.inflate(R.layout.disclaimer, null)
        dialogBuilder.setView(dialogView)

        val disclaimerEditText = dialogView.findViewById<TextView>(R.id.disclaimerText)
        val agreeButton = dialogView.findViewById<Button>(R.id.agreeButton)


        try {
            if (stateObj?.has(ACCEPT_TERMS_TEXT) == true) {
                disclaimerEditText.text = stateObj?.getString(ACCEPT_TERMS_TEXT)
            } else {
            }

            val dialog = dialogBuilder.create()

            agreeButton.setOnClickListener {
                dialog.dismiss()
            }


            dialog.show()

        } catch (e: JSONException) {
            Log.e("error is caused by: ", e)
        }

    }


    override val type = TYPE

    override fun validate(stateObj: JSONObject) {
        if (!stateObj.has(STORE_AS)) run {
            Log.e("validate: state object must have $STORE_AS defined")
            throw PolicyException("State object must have $STORE_AS defined")
        }
        else if (!stateObj.has(DISPLAY_MESSAGE)) {
            Log.e("validate: state object must have $DISPLAY_MESSAGE defined")
            throw PolicyException("State object must have $DISPLAY_MESSAGE defined")
        } else if (!stateObj.has(ON_NEXT)) {
            Log.e("validate: state object must have $ON_NEXT defined")
            throw PolicyException("State object must have $ON_NEXT defined")
        } else if (!stateObj.has(ON_CANCEL)) {
            Log.e("validate: state object must have $ON_CANCEL defined")
            throw PolicyException("State object must have $ON_CANCEL defined")
        }
    }


    companion object {
        private const val TYPE = "finger_signer"
        private const val ON_CANCEL = "on_cancel"
        private const val ON_NEXT = "on_next"
        private const val DISPLAY_MESSAGE = "display_message"
        private const val CANCEL_BUTTON_TEXT = "cancel_button_text"
        private const val NEXT_BUTTON_TEXT = "next_button_text"
        private const val CLEAR_BUTTON_TEXT = "clear_button_text"
        private const val ACCEPT_TERMS_TEXT = "accept_terms_text"
        private const val DISCLAIMER_SHORT_TEXT = "disclaimer_short_text"
        private const val STORE_AS = "store_as"

    }
}